import React from 'react';
import './Header.css';

function Header() {
  return (
    <header className="Header">
      <h1>Rampa Finder</h1>
      <nav>
        <ul>
          <li><a href="/">Mapa</a></li>
          <li><a href="/about">Información</a></li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;