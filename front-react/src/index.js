import * as React from 'react';
import ReactDOM from 'react-dom';
import Visor from './visor';
import Header from './Header'

ReactDOM.render(
    <React.StrictMode>
        <Header />
        <Visor />
    </React.StrictMode>,
    document.getElementById('root')
);