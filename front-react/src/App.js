import './App.css';

function App() {
  return (
    <div>
      <header className="header">
        <h1>Welcome to my simple main page!</h1>
      </header>
    </div>
  );
}

export default App;
