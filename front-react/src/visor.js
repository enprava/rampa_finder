import * as React from 'react';
import Map, { Source, Layer } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css'

const TOKEN = 'pk.eyJ1IjoiZW5wcmF2YSIsImEiOiJjbGdxaHhrdzEwMHNnM3JsOHF0NDJiY2VxIn0.6QtwODtt2jzwgCT-tzkKnQ';

export default function Visor() {
    const buildingsLayer = {
        id: 'buildings',
        type: 'fill',
        paint: {
            'fill-color': "green",
            'fill-opacity': 0.5
        }
    };
    const streetsLayer = {
        id: 'streets',
        type: 'line',
        paint: {
            'line-color': "green",
            // 'line-opacity': 0.5
        }
    };
    return <>
        <Map
            initialViewState={{
                longitude: -5.984241215329812,
                latitude: 37.38859470519951,
                zoom: 12
            }}
            style={{ height: '98vh' }}
            mapStyle="mapbox://styles/mapbox/streets-v11"
            mapboxAccessToken={TOKEN}
        >
            <Source type="geojson" data="http://localhost:8000/buildings/geojson/?format=json">
                <Layer {...buildingsLayer} />
            </Source>
            <Source type="geojson" data="http://localhost:8000/streets/geojson/?format=json">
                <Layer {...streetsLayer} />
            </Source>
        </Map>
    </>;

}
