from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import Street

class StreetSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Street
        geo_field = 'line'
        fields = ['name', 'address']