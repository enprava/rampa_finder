from django.contrib.gis.db import models


class Street(models.Model):
    name = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    line = models.LineStringField()

    def __str__(self):
        return f"{self.line} {self.name}"
