from rest_framework import generics
from .models import Street
from .serializers import StreetSerializer


class StreetGeoJSON(generics.ListAPIView, generics.CreateAPIView):
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
