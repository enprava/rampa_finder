import requests
import os
import json
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rampa_finder.settings")
django.setup()

from buildings.models import Building
from streets.models import Street

modelos = json.load(open("populate.json", "r", encoding="utf-8"))

for feature in modelos['features']:
    if feature['geometry']['type'] in 'Polygon':
        body = {
        "name": feature['properties']['nombre'],
        "address": feature['properties']['direccion'],
        "polygon": feature['geometry']
    }
        response = requests.post('http://127.0.0.1:8000/buildings/geojson/', json=body)
    else:
        body = {
        "name": feature['properties']['nombre'],
        "address": feature['properties']['direccion'],
        "line": feature['geometry']
    }
        response = requests.post('http://127.0.0.1:8000/streets/geojson/', json=body)
