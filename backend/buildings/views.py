from rest_framework import generics
from .models import Building
from .serializers import BuildingSerializer


class BuildingGeoJSON(generics.ListAPIView, generics.CreateAPIView):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer
