from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import Building


class BuildingSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Building
        geo_field = 'polygon'
        fields = ['name', 'address']
