from django.contrib.gis.db import models

class Building(models.Model):
    name = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    polygon = models.PolygonField()

    def __str__(self):
        return f"{self.polygon}: {self.name}"
