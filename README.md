# RampaFinder

RampaFinder is an application that geolocates wheelchair-friendly spaces for people with reduced mobility.

## Run this project

Create a .env file. You can use .env.template default configuration. Then run:
```
docker-compose up --build
```