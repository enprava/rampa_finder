const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    const filename = 'index.html';
    fs.readFile(filename, (err, data) => {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.write('Error 404: Archivo no encontrado.');
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            res.end();
        }
    });
});


server.listen(3000, '0.0.0.0', () => {
    console.log(`Server listenning on 0.0.0.0:3000`);
});
